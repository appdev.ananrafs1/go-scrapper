.DEFAULT_GOAL := brun
method = scrap
run :
	go run ./...
build :
	go build -o ./bin/go-scrapper.exe ./...
brun : build
	./bin/go-scrapper