package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/gocolly/colly"
)

func Scrap(dest string) Products {
	c := colly.NewCollector(colly.UserAgent("*"))

	pr := make(Products, 0)
	c.OnHTML(`div[data-testid="lstCL2ProductList"]`, func(e *colly.HTMLElement) {
		e.ForEach(`a[data-testid="lnkProductContainer"]`, func(index int, ch *colly.HTMLElement) {
			linkProduct := ch.Attr("href")
			var imgData string
			if !isPromo(linkProduct) {
				ch.ForEach("div img", func(indexImg int, ch *colly.HTMLElement) {
					if indexImg == 0 {
						imgData = ch.Attr("src")
					}
				})
				strContainer := make([]string, 0)
				ch.ForEach("div span", func(indexSpan int, ch *colly.HTMLElement) {
					strContainer = append(strContainer, ch.Text)
				})
				detailc := c.Clone()
				prodct := Product{Id: index, LinkImage: imgData, URLProduct: linkProduct}
				if strContainer[4] == "Cashback" || strContainer[4] == "Gratis Ongkir" {
					prodct.Name = strContainer[5]
					prodct.Price = strContainer[6]
					prodct.Merchant = strContainer[8]
				} else {
					prodct.Name = strContainer[4]
					prodct.Price = strContainer[5]
					prodct.Merchant = strContainer[7]
				}
				detailc.OnHTML(`div[data-testid="lblPDPDescriptionProduk"]`, func(e *colly.HTMLElement) {
					prodct.Description = e.Text
				})
				detailc.OnHTML(`span[data-testid="lblPDPDetailProductRatingNumber"]`, func(e *colly.HTMLElement) {
					fmt.Println(e.Text, e)
					rting, err := strconv.ParseFloat(e.Text, 32)
					if err != nil {
						log.Fatalf("cant Convert, %v", err)
					}
					prodct.Rating = float32(rting)
				})

				detailc.Visit(linkProduct)
				pr = append(pr, prodct)
			}

		})
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})
	c.OnScraped(func(r *colly.Response) {
		fmt.Println("Finished", r.Request.URL)
	})
	page := 1
	for {
		c.Visit(fmt.Sprintf("%s?ob=5&page=%d", dest, page))
		page++
		if len(pr) > 100 {
			break
		}

	}
	return pr
}

func isPromo(link string) bool {
	return strings.Contains(link, "ta.tokopedia")
}

func WriteScrap(w DataWriter, dest string) (int, error) {
	return w.Write(Scrap(dest))
}
