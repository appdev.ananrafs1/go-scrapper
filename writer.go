package main

import (
	"encoding/csv"
	"os"
)

type DataWriter interface {
	Write(Products) (int, error)
}

type CSVWriter struct {
	NameFile string
}

func (cw *CSVWriter) Write(pr Products) (i int, err error) {
	if len(cw.NameFile) == 0 {
		cw.NameFile = `cc.csv`
	}
	cFlie, err := os.Create(cw.NameFile)
	if err != nil {
		return 0, err
	}
	defer cFlie.Close()

	writer := csv.NewWriter(cFlie)
	defer writer.Flush()

	for _, v := range pr {
		err = writer.Write(v.AsStrings())
		i++
		if err != nil {
			return i, err
		}
	}
	return i, err

}
