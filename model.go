package main

import (
	"fmt"
)

type Product struct {
	Id          int     `json:"-"`
	URLProduct  string  `json:"-"`
	Name        string  `json:"name"`
	Description string  `json:"desc"`
	LinkImage   string  `json:"link"`
	Price       string  `json:"price"`
	Rating      float32 `json:"rating"`
	Merchant    string  `json:"merchant"`
}

func (pr *Product) AsStrings() []string {
	return []string{pr.Name, pr.Description, pr.LinkImage, pr.Price, fmt.Sprintf("%v", pr.Rating), pr.Merchant}
}

type Products []Product

func (pr Products) FillRating(id int, r float32, d string) {
	for _, v := range pr {
		if v.Id == id {
			v.Rating = r
			v.Description = d
			break
		}
	}
}
