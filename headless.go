package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/mxschmitt/playwright-go"
)

var (
	TimeOut    float64                   = 0
	isHeadless bool                      = true // skip using headless rn, try later
	waitState  playwright.WaitUntilState = *playwright.WaitUntilStateDomcontentloaded
	retry      int                       = 0
)

func ScrapHeadless(dest string) {
	pw, err := playwright.Run()
	if err != nil {
		log.Fatalf("could not start playwright: %v", err)
	}
	browser, err := pw.Chromium.Launch(playwright.BrowserTypeLaunchOptions{Timeout: &TimeOut, Headless: &isHeadless})
	if err != nil {
		log.Fatalf("could not launch browser: %v", err)
	}
	page, err := browser.NewPage()
	if err != nil {
		log.Fatalf("could not create page: %v", err)
	}
	_, err = page.Goto(dest, playwright.PageGotoOptions{WaitUntil: &waitState})
	if err != nil {
		log.Fatalf("could not goto: %v", err)
	}
	str, _ := page.Content()
	if strings.Contains(str, "Access Denied") {
		fmt.Println(str)
		return
	}
	entries, err := page.QuerySelectorAll(`a[data-testid="lnkProductContainer"]`)
	if err != nil {
		log.Fatalf("could not get entries: %v", err)
	}
	for {
		if len(entries) <= 10 && retry < 3 {
			err = page.Hover(`div.IOLazyloading`)
			if err != nil {
				fmt.Printf("cant perform action because %v \n", err)
			}
			entries, err = page.QuerySelectorAll(`a[data-testid="lnkProductContainer"]`)
			if err != nil {
				log.Fatalf("could not get entries: %v", err)
			}
			retry++
			continue
		}
		if retry >= 3 {
			log.Fatalf("%d Retry and still error, exit", retry)
		}
		break
	}

	for i, entry := range entries {
		if err != nil {
			log.Fatalf("could not get title element: %v", err)
		}
		title, err := entry.InnerText()
		if err != nil {
			log.Fatalf("could not get text content: %v", err)
		}
		fmt.Printf("%d: %s\n", i+1, title)
	}
	if err = browser.Close(); err != nil {
		log.Fatalf("could not close browser: %v", err)
	}
	if err = pw.Stop(); err != nil {
		log.Fatalf("could not stop Playwright: %v", err)
	}
}
